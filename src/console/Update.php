<?php
namespace Natenju\Admin\Console;

use Illuminate\Console\Command;

class Update extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = "installer:update";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Update Your app in command line";

    /**
     * handle
     *
     * @return void
     */
    public function handle()
    {
        $this->info("Launching your app update");
    }
}
