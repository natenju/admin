<?php

namespace Natenju\Admin\Console;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;

class Install extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "admin:install {parameters}";
    
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = "admin:install";
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Install Your Admin Panel inline";
    
    /**
     * handle
     *
     * @return void
     */
    public function handle() {
        
        $this->header();
        
        $this->checkRequirements();
        
        $this->info("Launching your app installation");
        
        if ( $this->confirm('Do you have setting the database configuration at .env ?') ) {
            
            $this->info('Dumping the autoloaded files and reloading all new files...');
            $composer = $this->findComposer();
            $process = new Process([$composer . ' dumpautoload'], base_path(), NULL, NULL, NULL);
            $process->run();
            
            $this->info('Migrating database...');
            $this->call('migrate');
            
            $this->call('config:clear');
            if ( app()->version() < 5.6 ) {
                $this->call('optimize');
            }
            
            #code...
            
            $this->info('Installing Your Admin Is Completed ! Thank You :)');
        } else {
            $this->info('Setup Aborted !');
            $this->info('Please setting the database configuration for first !');
        }
        
        $this->footer();
    }
    
    private function header() {
        $this->info("--------- setup installing" . env('APP_NAME', "App") . " ---------------");
    }
    
    private function checkRequirements() {
        $this->info('System Requirements Checking:');
        $system_failed = 0;
        $laravel = app();
        
        if ( $laravel::VERSION >= 5.3 ) {
            $this->info('Laravel Version (>= 5.3.*): [Good]');
        } else {
            $this->info('Laravel Version (>= 5.3.*): [Bad]');
            $system_failed++;
        }
        
        if ( is_writable(base_path('public')) ) {
            $this->info('public dir is writable: [Good]');
        } else {
            $this->info('public dir is writable: [Bad]');
            $system_failed++;
        }
        
        if ( $system_failed != 0 ) {
            $this->info('Sorry unfortunately your system did not meet with our requirements !');
            $this->footer(FALSE);
        }
    }
    
    /**
     * footer
     *
     * @param  mixed $success
     *
     * @return void
     */
    private function footer($success = TRUE) {
        $this->info('====================================================================');
        if ( $success == TRUE ) {
            $this->info('-------------------  Completed !!  ------------------------');
        } else {
            $this->info('-------------------    Failed !!   ------------------------');
        }
        exit;
    }
    
    private function findComposer() {
        if ( file_exists(getcwd() . '/composer.phar') ) {
            return '"' . PHP_BINARY . '" ' . getcwd() . '/composer.phar';
        }
    
        return 'composer';
    }
}
