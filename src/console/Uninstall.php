<?php

/**
 * @author Tchapga Nana Hermas
 * @email hermasn@yahoo.fr
 * @create date 2018-12-14 17:16:25
 * @modify date 2018-12-14 17:16:25
 * @desc [description]
 */

namespace Natenju\Admin\Console;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;

class Uninstall extends Command {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = "admin:uninstall";
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Uninstall Your app using command line";
    
    /**
     * handle
     *
     * @return void
     */
    public function handle() {
        
        $this->header();
        
        $this->checkRequirements();
        
        $this->warn("this will completely delete your app. continue");
        
        if ( $this->confirm("continue?") ) {
            $this->info("Launching your app uninstall");
            
            $composer = $this->findComposer();
            $process = new Process([$composer . ' dumpautoload'], base_path(), NULL, NULL, NULL);
            $process->run();
            
            $this->info('Rolling back database...');
            $this->call('migrate:rollback');
            
            $this->call('config:clear');
            $this->call('optimize');
            
            $this->info('Installing Your Admin Is Completed ! Thank You :)');
        }
        
        $this->footer();
    }
    
    private function header() {
        $this->info("--------- uninstalling" . env('APP_NAME', "App") . " ---------------");
    }
    
    /**
     * Get the composer command for the environment.
     *
     * @return string
     */
    protected function findComposer() {
        if ( file_exists(getcwd() . '/composer.phar') ) {
            return '"' . PHP_BINARY . '" ' . getcwd() . '/composer.phar';
        }
        
        return 'composer';
    }
    
    private function checkRequirements() {
    
    }
    
    private function footer($success = TRUE) {
        $this->info('====================================================================');
        if ( $success == TRUE ) {
            $this->info('-------------------  Completed !!  ------------------------');
        } else {
            $this->info('-------------------    Failed !!   ------------------------');
        }
        exit;
    }
}
