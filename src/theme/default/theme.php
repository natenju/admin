<?php
/**
 * @Author lenovo <${EMAIL}>.
 * @Created: 10 Jan 2019 00:46
 * @Updated: 10 Jan 2019 00:46
 * @Desc   : ${DESCRIPTION}
 */
// Theme configuration parameters to be published
return [
    'name'           => "AdminLTE",
    'version'        => 3,
    'author'         => [
        'name'  => "",
        'email' => "",
        'url'   => "",
    ],
    
    //the default layout.blade.php file to launch
    'default_layout' => "",
    'plugins'=>[],
    'description'    => "",
    'tags'           => [],
];