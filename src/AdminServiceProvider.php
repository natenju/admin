<?php
/**
 * User: lenovo
 * Date: 25 Dec 2018
 * Time: 08:54
 */

namespace Natenju\Admin;


use Illuminate\Support\ServiceProvider;
use Natenju\Admin\Console\Extract;
use Natenju\Admin\Console\Install;
use Natenju\Admin\Console\Uninstall;
use Natenju\Admin\Console\Update;

/**
 * Class AdminServiceProvider
 *
 * @package Natenju\Admin
 */
class AdminServiceProvider extends ServiceProvider {
    
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot() {
        $this->loadRoutesFrom(__DIR__ . "/routes.php");
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->loadTranslationsFrom(__DIR__ . '/resources/lang/', 'admin');
        $this->loadViewsFrom(__DIR__ . '/views', 'admin');
        
        if ( $this->app->runningInConsole() ) {
            $this->commands(
                [
                    Extract::class,
                    Install::class,
                    Uninstall::class,
                    Update::class,
                ]
            );
        }
        
        $this->publishes(
            [
                __DIR__ . "/assets" => public_path("vendor/admin"),
            ],
            "admin"
        );
    }
    
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register() {}
}