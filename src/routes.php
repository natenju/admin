<?php
/**
 * User: lenovo
 * Date: 25 Dec 2018
 * Time: 16:02
 */

Route::group(['as' => 'admin'],
    function () {
        $namespacePrefix = '\\' . config('natenju.controllers.namespace') . '\\';
        
        //Login LogOut Register
        Route::resource('auth',"AuthController");
        Route::group(['middleware' => 'admin.user'],
            function () use ($namespacePrefix){
            
        });
    }
);
Route::resource('/admin', 'AdminController');